package com.example.ejemplo1_1;

import java.io.Serializable;
import java.util.Random;

public class Cotizacion implements Serializable {


    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porcentajePagoInicial;
    private int plazos;



    //CONSTRUCTOR

    public Cotizacion(int folio, String descripcion, float valoAuto, float porcentajePagoInicial, int plazos) {
        this.folio = folio;
        this.descripcion = descripcion;
        this.valorAuto = valoAuto;
        this.porcentajePagoInicial = porcentajePagoInicial;
        this.plazos = plazos;
    }


    //VACIO

    public Cotizacion() {
        this.folio = this.generarId();
        this.descripcion = "";
        this.valorAuto = 0;
        this.porcentajePagoInicial = 0;
        this.plazos = 0;
    }

    //GETS


    public int getFolio() {
        return folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public float getPorcentajePagoInicial() {
        return porcentajePagoInicial;
    }

    public int getPlazos() {
        return plazos;
    }

    // SETS

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setValorAuto(float valoAuto) {
        this.valorAuto = valoAuto;
    }

    public void setPorcentajePagoInicial(float porcentajePagoInicial) {
        this.porcentajePagoInicial = porcentajePagoInicial;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }

    public int generarId(){
        Random r = new Random();
        return Math.abs(r.nextInt() % 1000);

    }

    public float calcularPagoInicial(){
        return this.valorAuto * (this.porcentajePagoInicial/100);

    }

    public float calcularPagoMensual(){
        return (this.valorAuto-this.calcularPagoInicial())/this.plazos;
    }



}
