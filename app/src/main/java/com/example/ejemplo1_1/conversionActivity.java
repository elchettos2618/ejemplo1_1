package com.example.ejemplo1_1;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.ArrayList;

public class conversionActivity extends AppCompatActivity {

    private EditText txtGrados;
    private ListView listaGrados;
    private TextView txtGradosResultado, txtTipoGrado;
    private  Button btnCalcularGrados, btnLimpiarGrados, btnCerrarGrados;

    private ArrayList<String> setArrayList(){
        ArrayList arrayList = new ArrayList();

        arrayList.add("Celsuis a Fahrenheit");
        arrayList.add("Fahrenheit a Celsius");

        return arrayList;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_conversion);

        iniciarComponentes();
        ArrayList<String> stringArrayList = setArrayList();
        ArrayAdapter<String> arrayAdapter= new
                ArrayAdapter<>(conversionActivity.this,
                android.R.layout.simple_list_item_single_choice,
                stringArrayList);

        listaGrados.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listaGrados.setAdapter(arrayAdapter);


        btnCalcularGrados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int validarSeleccion = listaGrados.getCheckedItemPosition();
                if (validarSeleccion == AdapterView.INVALID_POSITION){
                    Toast.makeText(conversionActivity.this, "Seleccione una conversión",
                            Toast.LENGTH_SHORT).show();
                } else if (txtGrados.getText().toString().matches("")) {
                    Toast.makeText(conversionActivity.this, "Ingrese un numero",
                            Toast.LENGTH_SHORT).show();
                } else if(validarSeleccion == 0){

                    float grados = Float.parseFloat(txtGrados.getText().toString());
                    float gradosFar = (grados*9/5)+32;
                    txtGradosResultado.setText(String.valueOf(gradosFar));
                    txtTipoGrado.setText("C°");
                    Toast.makeText(conversionActivity.this, "Conversión realizada",
                            Toast.LENGTH_SHORT).show();

                } else if (validarSeleccion == 1){
                    float grados = Float.parseFloat(txtGrados.getText().toString());
                    float gradosCel = (grados-32)*5/9;
                    txtGradosResultado.setText(String.valueOf(gradosCel));
                    txtTipoGrado.setText("F°");
                    Toast.makeText(conversionActivity.this, "Conversion realizada",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnLimpiarGrados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtGrados.setText("");
                txtTipoGrado.setText("");
                txtGradosResultado.setText("0.0");
                listaGrados.clearChoices();
                listaGrados.requestLayout();
            }
        });

        btnCerrarGrados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }



    public void iniciarComponentes(){

        listaGrados = (ListView) findViewById(R.id.listaGrados);
        txtTipoGrado = (TextView) findViewById(R.id.txtTipoGrado);
        txtGrados = (EditText) findViewById(R.id.txtGrados);
        txtGradosResultado = (TextView) findViewById(R.id.txtGradosResultado);
        btnCalcularGrados = (Button) findViewById(R.id.btnCalcularGrados);
        btnLimpiarGrados = (Button) findViewById(R.id.btnLimpiarGrados);
        btnCerrarGrados = (Button) findViewById(R.id.btnCerrarGrados);

    }

}